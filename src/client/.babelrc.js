module.exports = function (api) {
  api.cache(false);

  const presets = [
    [
      "@babel/preset-env",
      {
        modules: false,
        shippedProposals: true,
        bugfixes: true
      }
    ],
    [
      "@babel/preset-react",
      {
        runtime: "automatic"
      }
    ]
  ];

  const plugins = [
    ["@babel/plugin-syntax-dynamic-import"],
    ["jsx-control-statements"],
    [
      "babel-plugin-import",
      {
        libraryName: "@material-ui/core",
        libraryDirectory: "esm",
        camel2DashComponentName: false
      },
      "core"
    ],
    [
      "babel-plugin-import",
      {
        libraryName: "@material-ui/icons",
        libraryDirectory: "esm",
        camel2DashComponentName: false
      },
      "icons"
    ],
    [
      "babel-plugin-import",
      {
        libraryName: "lodash",
        libraryDirectory: ".",
        camel2DashComponentName: false
      },
      "lodash"
    ]
  ];

  return {
    presets,
    plugins,
    sourceMaps: "inline"
  };
};
