/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {createTheme} from "@mui/material/styles";

const black = "#000000";
const mainGrey = "#aeadad";
const orange = "#ffdc8c";
const white = "#ffffff";
const green = "#a0aa87";
const pink = "#fbdce0";
const blue = "#a0b4ff";
const linkColor = green;

export const theme = createTheme({
  palette: {
    primary: {
      // light: will be calculated from palette.primary.main,
      main: green
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
    },
    secondary: {
      main: green,
      contrastText: "#FFFFFF"
    },
    // Used by `getContrastText()` to maximize the contrast between
    // the background and the text.
    contrastThreshold: 3,
    // Used by the functions below to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.2,
    text: {
      emptyHint: "#474747"
    }
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 700,
      lg: 1080,
      xl: 1920
    }
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        a: {
          color: linkColor
        }
      }
    },
    MuiAppBar: {
      styleOverrides: {
        colorPrimary: {
          backgroundColor: white,
          color: black
        }
      }
    },
    MuiLink: {
      styleOverrides: {
        root: {
          color: linkColor
        }
      }
    },
    MuiIconButton: {
      styleOverrides: {
        colorPrimary: {
          color: blue
        },
        colorInherit: {
          color: black
        }
      }
    },
    MuiButton: {
      styleOverrides: {
        containedPrimary: {
          backgroundColor: white,
          color: black,
          "&:hover": {
            backgroundColor: white
          }
        }
      }
    },
    MuiTimelineItem: {
      styleOverrides: {
        missingOppositeContent: {
          "&:before": {
            display: "none"
          }
        }
      }
    }
  }
});
