export const en = {
  // ACTIONS: {
  //   ADD_SUB_PROJECT: 'Add a child investigation',
  //   ADD_PARENT_PROJECT: 'Add a parent investigation',
  //   RICH_TEXT_EDITOR: {
  //     MENTION_BUTTON: {
  //       PROJECT: 'Mention an investigation',
  //     },
  //   }
  // },
  // ARTISTIC_OBJECT: {
  //   PROJECTS: 'Related investigations'
  // },
  // BIN: {
  //   ACTIONS: {
  //     LINK_TO_NEW_PROJECT_CONTRIBUTION: {
  //       DESCRIPTION: 'You are going to link these resources to a new event in the investigation {{project}}'
  //     },
  //     LINK_TO_PROJECT: {
  //       DESCRIPTION: 'You are about to reassign one or more items to a new investigation.',
  //       TITLE: 'Link to an investigation',
  //     },
  //     LINK_TO_PROJECT_CONTRIBUTION: {
  //       DESCRIPTION: 'You are going to link these resources to an investigation event {{project}}.',
  //     }
  //   }
  // },
  // BREADCRUMB: {
  //   PROJECTS: 'List of investigations'
  // },
  // IMPORT: {
  //   FEATURES_PROJECTS: 'Latest selected investigations :',
  // },
  // PROJECT: {
  //   AUTOCOMPLETE: {
  //     CHOOSE: 'Select an investigation...',
  //     CHOOSE_ANOTHER: 'Select another investigation...',
  //     NO_RESULT: 'No investigation found.',
  //   },
  //   CREATE: 'New investigation',
  //   CREATE_TITLE: 'Create a new investigation',
  //   END_DATE_TOOLTIP: "Date of last investigation's event",
  //   NEW: 'Add an investigation',
  //   NO_RELATED_SUBPROJECTS: 'No related sub-investigations',
  //   START_DATE_TOOLTIP: "Date of first investigation's event",
  //   SUB_LABEL: 'Select one or more investigations to link',
  //   SUB_PROJECTS: 'Sub-investigations',
  //   NO_PARENT_PROJECT: 'None',
  //   PARENT_PROJECT: "Parent's investigation"
  // },
  // SEARCH: {
  //   COLUMNS: {
  //     COMMON: {
  //       IS_PARENT_PROJECT: 'Root investigation',
  //       PROJECT: 'Investigation'
  //     },
  //     PROJECTS: {
  //       GATHER_SUB_PROJECTS: 'Gather sub investigations',
  //     },
  //   },
  //   TABS: {
  //     PROJECTS: 'Investigations'
  //   },
  // },
  // TYPENAME: {
  //   PROJECT: 'Investigation'
  // }
};
