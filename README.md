# Weever for Atelier Luma

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![npm version](https://badge.fury.io/js/%40mnemotix%2Fsynaptix.js.svg)](https://badge.fury.io/js/%40mnemotix%2Fsynaptix.js)


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Description](#description)
- [Commandes](#commandes)
  - [Lancement d'un environnement local](#lancement-dun-environnement-local)
  - [Installation](#installation)
  - [Démarrage de l'application](#d%C3%A9marrage-de-lapplication)
    - [En mode production](#en-mode-production)
    - [En mode développement](#en-mode-d%C3%A9veloppement)
      - [Variables d'environnements](#variables-denvironnements)
  - [Tests](#tests)
- [Utilisation](#utilisation)
  - [Sécurisation des resolvers](#s%C3%A9curisation-des-resolvers)
  - [Application frontend](#application-frontend)
    - [Mode développement](#mode-d%C3%A9veloppement)
    - [Mode production](#mode-production)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Description

L'application Weever est un serveur NodeJS offrant d'une part un point d'entrée public sur le HUB de données. Pour cela, il démarre :

- Un point d'accès SSO pour pouvoir obtenir un jeton d'authentification.
- Un point d'accès GraphQL pour délivrer de la donnée dans un format standardisé.
- Un point d'accès SPARQL pour accéder directement au Triple Store. (A venir)

Et d'autre part servant de serveur web minimaliste pour l'application frontend. Pour plus de détail voir le [README](./src/client/README.md) 
consacré à l'application frontend React.

## Commandes

### Lancement d'un environnement local

Avant le lancer l'application, il faut un environnement de développement fonctionnel :

Au premier déploiement, installer le système de fichiers pour les volumes permanents Docker :

```
./launcher/console install
```

Puis lancer la stack :

```
./launcher/console start
```

### Installation

Installer Yarn et lancer la commande : 

```
yarn install
```

Elle a pour effet d'installer toutes les dépendances.

### Démarrage de l'application 

#### En mode production

Lancer d'abord la commande de build (compile les sources frontend)

```
yarn build:prod
```

Puis lancer la commande : 

```
yarn start:prod
```

#### En mode développement

Lancer la commande :

```
yarn start
```

Elle démarre un serveur ExpressJS sur le port 3034 (par défaut, voir la section suivante pour le changer) avec un endpoint GraphQL.

##### Variables d'environnements

Il est possible avant le démarrage d'initialiser vos propres variables d'environnements.

La liste est définie dans le fichier [./config/environment.js](./config/environment.js)

Par exemple pour changer le port d'écoute du serveur, il suffit de lancer :

```
APP_PORT=80 yarn start
```

Vous pouvez également définir une liste de variable à utiliser dans le fichier `.env` à la racine (fichier non versionné). Pour que ce fichier
soit pris en compte vous devez définir la variable `USE_DOTENV`. C'est le comportement par défaut lorsqu'on lance l'application en mode production
avec `yarn start:prod`.

**Example :**

- fichier `.env`

```
APP_PORT=80
RABBITMQ_HOST=localhost
RABBITMQ_PORT=5672
```

- lancement de l'application

```
USE_DOTENV=1 yarn start
```

### Tests

Lancer la suite de test 

```
yarn test
```

Lancer la suite de test en mode debuggage, en utilisant chrome developper tools comme client de debuggage.

```
yarn test:debug
```

## Utilisation

L'application est largement basée sur la bibliothèque [Synaptix.js](https://gitlab.com/mnemotix/synaptix.js). La partie complémentaire de la documentation est disponible dans son fichier [README](https://gitlab.com/mnemotix/synaptix.js/blob/master/README.md).

### Sécurisation des resolvers

Nous allons utiliser la bibliothèque [GraphQL Shield](https://github.com/maticzav/graphql-shield) pour sécuriser les resolvers avec des middlewares.


### Application frontend

L'application frontend est une Single Page Application, qui fonctionne avec le framework React. Les sources frontend (javascript et assets) sont
compilées & bundlées par Webpack. Webpack est configuré via un fichier unique [`webpack.config.js`](./webpack.config.js), pour le mode développement
et production. Le middleware [`serveFrontend`](./src/server/middlewares/serveFrontend.js) est responsable de servir le frontend.

#### Mode développement

Le serveur NodeJS configure et lance webpack-dev-server pour bundler en live et servir l'application frontend. 

#### Mode production

En mode production, il faut préalablement compiler/bundler les sources frontend avec webpack, ceci est fait en exécutant le script 

```
yarn build:prod
```

Les sources compilées se trouvent dans le dossier `/dist` (non versionné).

Le serveur nodeJS lancé en mode production (`yarn start:prod`) sert statiquement les fichiers du dossier `/dist`, et pour toute route 
non utilisée, sert le fichier `index.html` par défaut (ce qui permet la navigation en mode Single Page Application).