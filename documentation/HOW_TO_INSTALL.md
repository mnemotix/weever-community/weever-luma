# Pour installer la stack Weever sur l'infrastructure LUMA

## Pré-requis

Pour fonctionner, la stack Weever a besoin :

- Un environnement Docker/Docker-compose
- Des bases de données installées (voir partie suivante)
- Des enregistrements DNS fonctionnels (voir partie enregistrements DNS) pointant sur l'infra hébergeant Weever
- Un reverse proxy faisant la passerelle entre les enregistrements DNS et les conteneurs Docker de la stack (voir partie Reverse Proxy)
- Un certificat SSL valide utilisé par le reverse proxy
- Un système de fichier NFS (ou du moins sauvegardé) pour placer les fichiers importés via Weever.

Note : La documentation suivante donne un exemple de configuration pour :

- Une machine (ou VM) avec un OS Linux.
- Apache2 comme reverse proxy.

## Installation

### Installer les bases de données.

La stack a besoin de 3 bases de données pour fonctionner :

- [GraphDB EE](https://graphdb.ontotext.com/documentation/enterprise/). Attention, une licence est requise pour l'édition entreprise, Mnemotix vous la fournira.
- [Elastic Search >7](https://www.elastic.co/fr/)
- Mysql pour le serveur Keycloak

Nota bene : Le serveur Keycloak est lancé dans un conteneur Docker, il faut donc que ce conteneur puisse communiquer avec Mysql.

### Configurer Mysql

Mysql est disponible depuis la machine hôte et non pas comme un service Docker. Il faut donc faire une manip pour qu'un conteneur puisse y accéder. Celle-ci consiste à lier l'adresse IP d'écoute (par défaut localhost) à l'IP de gateway configurée dans le fichier docker-compose.

- Modifier le fichier my.cnf pour faire un bind-address sur l'IP de gateway précisée dans la donc docker-compose (172.18.0.1)

#### **`/etc/mysql/my.conf`**
```conf
!includedir /etc/mysql/conf.d/
!includedir /etc/mysql/mysql.conf.d/

[mysqld]

bind-address = 172.18.0.1
```

###  Déployer les fichiers de conf sur la machine

Dans le répertoire `/var/www/weever`, copier les fichiers :

- `templates/docker-compose.yml` : Le fichier docker compose prêt à l'emploi permettant le démarrage de la stack de conteneurs applicatifs.
- `templates/apache-vhosts.conf` : Le fichier de config Apache prêt à l'emploi permettant de créer un Proxy/ReverseProxy vers les différents conteneurs applicatifs.
- `templates/.env` : Le fichier d'environnement à configurer avec les bons paramètres

Note: Dans le fichier docker-compose, des addresses IP statiques (sous réseau 172.18.0.0) sont assignées aux différents conteneurs à mettre en frontal sur le web. Ces Ips doivent correspondrent dans le fichier des vhosts Apache.

### Configurer les variables d'environnement

 - Editer le fichier `/var/www/weever/.env` pour configurer les variables d'environnement
   
Note: Les deux variables `MINIO_DATA_PATH` et `THUMBOR_DATA_PATH` se réfèrent à des volumes dockers qui peuvent contenir une masse de fichiers importante au fur et à mesure de l'utilisation. Privilégier donc un montage NFS sur un NAS.

 - Editer le fichier `/var/www/weever/apache-vhosts.conf` pour configurer la configuration d'Apache2 en mode Reverse-Proxy et l'ajouter aux vhosts d'Apache2.

Note : Les enregistrements DNS doivent être prêts pour cette étape.

### Configurer Apache

Ajouter le module mod_headers:

```sh
a2enmod headers
```

Ajouter le fichier de configuration Apache :

```sh
 ln -s /var/www/weever/apache-vhosts.conf /etc/apache2/sites-enabled/apache-vhosts.conf
```

Redémarrer Apache :

```sh
service apache2 restart
```

### Configurer GraphDB

- Créer un nouveau repository dans GraphDB (via son outil en ligne Workbench)

Le nom du repository correspond à la variable d'environnement à configurer (partie suivante):

```
GRAPHDB_REPOSITORY=
```

- Créer un nouvel utilisateur à associer en LECTURE / ECRITURE à ce repository. Cet utilisateur correspond aux variables d'environnement à configurer (partie suivante):

```
GRAPHDB_USER=
GRAPHDB_PASSWORD=
```

- Importer l'ontologie via le [workbench](https://URL_DE_GRAPHDB/import#user). Cliquer sur "Upload RDF files" et charger le fichier `data/graphdb/ontology.trig`. Laisser les options d'import par défaut.


## Lancer la stack

### En passant par docker-compose

Dans le dossier du docker-compose.yml :

```sh
  docker-compose up -d 
```

## Finalisation de configuration après le premier démarrage

Quelques étapes manuelles restent nécéssaires pour finaliser la configuration.

### Keycloak

Se connecter à Keycloak avec les identifiants disponibles dans le `.env`

Créer un nouveau Realm en important le fichier `templates/keycloak.json` APRES avoir changé 

 - la ligne 7 :

```
"secret": "** NE PAS OUBLIER DE REMPLIR LE MÊME SECRET QUE DANS LE .env",
```

Mettre dans cette ligne la même valeur que celle configurée dans la variable `KEYCLOAK_CLIENT_SECRET` du `.env` après l'avoir générée sur [un UUID générateur](https://www.uuidgenerator.net/version4)

 - les lignes 54-58 pour régler le serveur SMTP d'envoi de mail de réinitialisation de mot de passe.

### Minio

Se connecter à Minio avec les identifiants disponibles dans le `.env`  (`MINIO_ACCESS_KEY_ID`, `MINIO_SECRET_ACCESS_KEY`)

Créer un nouveau bucker nommé du même nom que la variable d'environnement `COMPOSE_PROJECT_NAME`.


### Mise en place des connecteurs GraphDB => ES

Lancer la commande :

```bash
docker exec -ti weever yarn data:index -a
```




